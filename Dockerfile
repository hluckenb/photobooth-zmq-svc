FROM ruby:2.7.3

WORKDIR /app
COPY . /app
RUN apt-get update
RUN apt-get install -y -qq libzmq3-dev
RUN bundle install

EXPOSE 4567

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "4567"]
