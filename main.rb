require 'sinatra'
require 'ffi-rzmq'

get '/' do
  'It works!'
end

get '/shoot' do
  headers 'Access-Control-Allow-Origin' => '*'
  content_type :json

  begin
    ctx = ZMQ::Context.new
    req = ctx.socket(ZMQ::REQ)
    req.connect(ENV['DOCKER'] ? 'tcp://host.docker.internal:54544' : 'tcp://127.0.0.1:54544')
    req.send_string({
      msg_type: "Request",
      msg_id: "SetShutterButton",
      msg_seq_num: 0,
      CameraSelection: "All",
      CameraShutterButton: 'full'
    }.to_json)

    reply = ''
    req.recv_string(reply)

    puts reply

    sleep 4
  ensure
    req.send_string({
      msg_type: "Request",
      msg_id: "SetShutterButton",
      msg_seq_num: 0,
      CameraSelection: "All",
      CameraShutterButton: 'off'
    }.to_json)

    reply = ''
    req.recv_string(reply)

    puts reply
  end

  { success: true }.to_json
end
